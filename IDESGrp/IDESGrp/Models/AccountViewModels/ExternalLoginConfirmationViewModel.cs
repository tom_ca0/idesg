﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace IDESGrp.Models.AccountViewModels
{
    public class ExternalLoginConfirmationViewModel
    {
        [Required]
        [EmailAddress]
        public string Email { get; set; }
        public string Name { get; set; }
        public string NameIdentifier { get; set; }
        public string DateOfBirth { get; set; }
        public string Country { get; set; }
        public string HomePhone { get; set; }
    }
}
