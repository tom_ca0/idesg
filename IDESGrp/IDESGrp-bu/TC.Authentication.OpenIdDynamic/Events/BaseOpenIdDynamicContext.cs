// BaseOpenIdDynamicContext.cs

using System;
using Microsoft.AspNetCore.Authentication;  // for BaseControlContext
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.IdentityModel.Protocols.OpenIdConnect;

namespace TC.Authentication.OpenIdDynamic
{
    public class BaseOpenIdDynamicContext : BaseControlContext
    {
        public BaseOpenIdDynamicContext(HttpContext context, OpenIdDynamicOptions options)
            : base(context)
        {
            if (options == null)
            {
                throw new ArgumentNullException(nameof(options));
            }

            Options = options;
        }

        public OpenIdDynamicOptions Options { get; }

        public OpenIdConnectMessage ProtocolMessage { get; set; }
    }
}