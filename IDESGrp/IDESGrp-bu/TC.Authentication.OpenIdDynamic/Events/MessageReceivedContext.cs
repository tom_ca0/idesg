// Copyright (c) .NET Foundation. All rights reserved.
// Licensed under the Apache License, Version 2.0. See License.txt in the project root for license information.

using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Authentication;

namespace TC.Authentication.OpenIdDynamic
{
    public class MessageReceivedContext : BaseOpenIdDynamicContext
    {
        public MessageReceivedContext(HttpContext context, OpenIdDynamicOptions options)
            : base(context, options)
        {
        }

        /// <summary>
        /// Bearer Token. This will give application an opportunity to retrieve token from an alternation location.
        /// </summary>
        public string Token { get; set; }

        public AuthenticationProperties Properties { get; set; }
    }
}